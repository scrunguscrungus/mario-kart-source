#ifndef C_MKSCLIENTKART_H
#define C_MKSCLIENTKART_H

#include "mks/Kart/c_mks_cube.h"
#include "mks/Kart/mks_kart_profile.h"

class C_MKSPlayer;
class C_MKSEngine;
class C_MKSKartMovement;
class C_MKSKart;

#define MKS_DEFAULT_GRAVITY Vector(0,0,1)

class C_MKSClientKart : public C_MKSCube
{
public:
	DECLARE_CLASS( C_MKSClientKart, C_MKSCube );

//Functions
public:
	C_MKSClientKart();
	~C_MKSClientKart();

	bool InitializeKart( void );

	virtual bool InitProfile( void );
	virtual void GetProfileIndexes( MKSPlayerProfile *pProfile );

	virtual bool InitializePhysics( void );
	virtual void SetPhysicsParams( void );
	virtual void CreateMinMax( void );
	
	virtual void UpdateValues( void );
	virtual void Update( void );

	virtual void CalculateWheelData( void );

	virtual void Drive( void );

	//void VPhysicsCollision( int index, gamevcollisionevent_t *pEvent );
	virtual unsigned int	PhysicsSolidMaskForEntity( void ) const;

	virtual void Precache( void );
	virtual void Spawn( void );
	virtual void SpawnClientEntity( void );

	static C_MKSClientKart *CreateKart( CMKSPlayer *pOwner, const char *szModel );

//Accessors
public:
	virtual CMKSPlayer *GetDriver(void);
	virtual void SetDriver( CMKSPlayer *mksPlayer );

	virtual MKSPlayerProfile *GetProfile() { return m_pProfile; };
	virtual void SetProfile( MKSPlayerProfile *profile ) { m_pProfile = profile; };//const char *szKartName );

	virtual C_MKSEngine *GetEngine() { return m_pEngine; };
	virtual void SetEngine( C_MKSEngine *pEngine ) { m_pEngine = pEngine; };

	virtual C_MKSKart *GetServerKart() { return m_pServerKart; };
	virtual void SetServerKart( C_MKSKart *pKart ) { m_pServerKart = pKart; };

	virtual int GetWheelGroundCount( void ) { return m_iWheelGroundCount; };
	virtual void SetWheelGroundCount( int iWheelCnt ) { m_iWheelGroundCount = iWheelCnt; };

	virtual int WheelCount( void ) { return m_pWheels.Count(); };
	virtual CMKSWheel *GetWheel( int index ) { return m_pWheels.Element(index); };
	virtual int AddWheel( CMKSWheel *pWheel ) { return m_pWheels.AddToTail( pWheel ); };
	virtual void RemoveWheel( int iWheel ) { m_pWheels.Remove( iWheel ); };

	virtual void AddVelocity( Vector vVel ) { VectorAdd( vVel, m_vAddVelocity, m_vAddVelocity ); };
	virtual Vector GetAddVelocity() { return m_vAddVelocity; };
	virtual void ClearAddVelocity() { m_vAddVelocity = Vector(0,0,0); };

	virtual AngularImpulse GetAngularVelocity() { return m_angVelocity; };
	virtual void SetAngularVelocity( AngularImpulse angVel ) { m_angVelocity = angVel; };

	virtual Vector GetGravityDirection() { return m_vGravityDirection; };
	virtual void SetGravityDirection( Vector vVel ) { m_vGravityDirection = vVel; };

	virtual Vector GetWorldOrigin() { return m_vWorldOrigin; };
	virtual void SetWorldOrigin( Vector vVel ) { m_vWorldOrigin = vVel; };

	virtual Vector GetForward() { return m_vForward; };
	virtual Vector GetRight() { return m_vRight; };
	virtual Vector GetUp() { return m_vUp; };
	virtual matrix3x4_t GetKartMatrix() { return m_vMatWorld; };


	virtual float GetKartVelocity() { return m_flKartVelocity; };
	virtual void SetKartVelocity( float flKartVel ) { m_flKartVelocity = flKartVel; };

	virtual float GetKartSpeed() { return m_flKartSpeed; };
	virtual void SetKartSpeed( float flSpeed ) { m_flKartSpeed = flSpeed; };

	virtual float GetMaxKartSpeed() { return m_flMaxKartSpeed; };
	virtual void SetMaxKartSpeed( float flSpeed ) { m_flMaxKartSpeed = flSpeed; };

	virtual float GetThrottle() { return m_flThrottle; };
	virtual void SetThrottle( float flThrottle ) { m_flThrottle = flThrottle; };

	virtual float GetBrake() { return m_flBrake; };
	virtual void SetBrake( float flBrake ) { m_flBrake = flBrake; };

	virtual float GetSteering() { return m_flSteering; };
	virtual void SetSteering( float flSteering ) { m_flSteering = flSteering; };

	virtual bool GetSliding() { return m_bSliding; };
	virtual void SetSliding( bool bSliding ) { m_bSliding = bSliding; };

	virtual float GetWheelFriction() { return m_flFriction; };
	virtual void SetWheelFriction( float flFriction ) { m_flFriction = flFriction; };


//Server Variables
public:
	float m_flKartVelocity;

	float m_flKartSpeed;
	float m_flMaxKartSpeed;

	float m_flThrottle;
	float m_flBrake;

	float m_flSteering; 

	bool m_bSliding; //Kart Sliding parameter

	float m_flFriction; //Kart Friction against road

//Offline Variables
public:

	C_MKSKartMovement *m_pMovement;

	C_MKSKart *m_pServerKart;

	C_MKSPlayer *m_pDriver;
	C_MKSEngine *m_pEngine;

	MKSPlayerProfile *m_pProfile;

	int m_iWheelGroundCount;
	int m_iWheelCount;
	CUtlVector< CMKSWheel * > m_pWheels;

	Vector m_vGravityDirection;
	Vector m_vAddVelocity;
	AngularImpulse m_angVelocity;

	matrix3x4_t m_vMatWorld;

	Vector m_vWorldOrigin;
	Vector m_vForward;
	Vector m_vRight;
	Vector m_vUp;

};

#endif