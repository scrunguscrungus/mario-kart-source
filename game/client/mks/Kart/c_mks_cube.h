//--------------------------------------------------------------------------------------
//
//
//
//
//--------------------------------------------------------------------------------------

#ifndef MKS_CUBE_H
#define MKS_CUBE_H

class C_MKSPlayer;
#include "mks/Kart/mks_kart_profile.h"

class C_MKSCube : public C_BaseAnimating
{
public:
	//typedef C_BaseAnimating BaseClass;
	DECLARE_CLASS( C_MKSCube, C_BaseAnimating );
	DECLARE_CLIENTCLASS();

public:

	C_MKSCube();
	~C_MKSCube();

	//DECLARE_PREDICTABLE();
	

	virtual bool InitializePhysics( void );
	virtual void SetPhysicsParams( void );

	virtual int DrawModel(int flags);

	virtual void CreateMinMax( void );
	virtual void ApplyBoneMatrixTransform( matrix3x4_t& transform );

	virtual void OnDataChanged( DataUpdateType_t updateType );
	virtual void PostDataUpdate( DataUpdateType_t updateType );


	//virtual void CopyCube( C_MKSCube *pCopy );
	

public:
	bool m_bCreated;
	
	//MKSPlayerProfile *m_pProfile;

	Vector m_vCubeMins;
	Vector m_vCubeMaxs;
	Vector m_vCubeScale;


	float m_flDamping;
	float m_flRotDamping;
	float m_flDrag;
	float m_flMass;
	float m_flRotDrag;
	float m_flInertiaX;
	float m_flInertiaY;
	float m_flInertiaZ;
};

#endif //MKS_CUBE_H