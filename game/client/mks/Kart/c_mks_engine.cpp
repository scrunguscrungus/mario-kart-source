#include "cbase.h"
#include "mks/Kart/c_mks_engine.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_clientkart.h"
#include "mks/Rules/mks_cvar_settings.h"

#include "in_buttons.h"

C_MKSEngine::C_MKSEngine()
{
	m_iButtons = 0;
	m_iOldButtons = 0;

	m_iSlideDirection = 0;
	m_bCanReverse = false;
}

C_MKSEngine::~C_MKSEngine()
{
}


C_MKSEngine *C_MKSEngine::CreateEngine( C_MKSClientKart *pOwner )
{
	C_MKSEngine *pEngine = new C_MKSEngine();
	
	if( pEngine == NULL )
		return NULL;

	pEngine->SetKart( pOwner );
	return pEngine;
}


void C_MKSEngine::Update( void )
{
	C_MKSClientKart *mksKart = GetKart();
	C_MKSPlayer *mksPlayer = mksKart->GetDriver();
	CUserCmd *mksCmd = mksPlayer->GetCurrentUserCmd();

	m_iButtons = mksCmd->buttons;

	Pedals( mksCmd );
	Steering( mksCmd );
	Jump( mksCmd );
}


C_MKSClientKart *C_MKSEngine::GetKart( void )
{
	return m_pKart;
}

void C_MKSEngine::Pedals( CUserCmd *pCmd )
{
	float flForward = pCmd->forwardmove;
	
	flForward = clamp( flForward, -1.0f, 1.0f );

	if( flForward > 0.0f )
	{
		Gas( pCmd );
	}
	else if( flForward < 0.0f )
	{
		Brake( pCmd );
	}
	else
	{
		Neutral( pCmd );
	}
}


void C_MKSEngine::Gas( CUserCmd *pCmd )
{
	float flForward = pCmd->forwardmove;
	float flCurrentSpeed = GetKart()->GetKartSpeed();
	float flSpeed = mks_forward_speed.GetFloat(); 
	float flMaxSpeed = mks_forward_maxspeed.GetFloat();

	flForward = clamp( flForward, -1.0f, 1.0f );
	flCurrentSpeed += flSpeed * gpGlobals->frametime * flForward;
	flCurrentSpeed = clamp( flCurrentSpeed, 0.0f, flMaxSpeed );

	m_bCanReverse = false;

	GetKart()->SetKartSpeed( flCurrentSpeed );
}


void C_MKSEngine::Brake( CUserCmd *pCmd )
{
	float flForward = pCmd->forwardmove;
	float flCurrentSpeed = GetKart()->GetKartSpeed();
	float flSpeed = mks_reverse_speed.GetFloat();
	float flMaxSpeed = mks_reverse_maxspeed.GetFloat();

	if( !m_bCanReverse ) 
	{
		float flVelocity = GetKart()->GetKartVelocity();
		if(flVelocity < 50.0f)
			m_bCanReverse = true;

		flSpeed *= 0.5f;
	} 

	flForward = clamp( flForward, -1.0f, 1.0f );
	flCurrentSpeed += flSpeed * gpGlobals->frametime * flForward;
	flCurrentSpeed = clamp( flCurrentSpeed, -flMaxSpeed, flMaxSpeed );

	GetKart()->SetKartSpeed( flCurrentSpeed );
}

void C_MKSEngine::Neutral( CUserCmd *pCmd )
{
	float flCurrentSpeed = GetKart()->GetKartSpeed();
	float flSpeed = mks_neutral_decayspeed.GetFloat();
	float flOffset = -1.0f;

	if( flCurrentSpeed < 0.0f )
		flOffset = 1.0f;

	flCurrentSpeed += flSpeed * gpGlobals->frametime * flOffset;

	if( fabs(flCurrentSpeed) < 1.0f )
		flCurrentSpeed = 0.0f;

	GetKart()->SetKartSpeed( flCurrentSpeed );
}


void C_MKSEngine::Steering( CUserCmd *pCmd )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart )
		return;

	float flSteer = pCmd->sidemove;
	float flCurrentSteer = mksKart->GetSteering();
	float flSteerSpeed = mks_steering_speed.GetFloat();
	float flSteerMin = mks_steering_min.GetFloat();
	float flSteerMax = mks_steering_max.GetFloat();
	float flSpeed = mksKart->GetKartSpeed();
	bool bSliding = mksKart->GetSliding();

	flSteer = clamp( flSteer, -1.0f, 1.0f );

	if( flSpeed > 0.0f )
		flSteer *= -1.0f;

	flCurrentSteer += flSteer * flSteerSpeed * gpGlobals->frametime;

	if( flSteer == 0.0f )
	{
		if( flCurrentSteer < 0.0f )
		{
			flCurrentSteer += flSteerSpeed * gpGlobals->frametime;
			if( flCurrentSteer > 0.0f )
				flCurrentSteer = 0.0f;
		}
		else if( flCurrentSteer > 0.0f )
		{
			flCurrentSteer -= flSteerSpeed * gpGlobals->frametime;
			if( flCurrentSteer < 0.0f )
				flCurrentSteer = 0.0f;
		}

	}

	float *flSteerEdit = &flSteerMin;
	if( m_iSlideDirection == 1 )
	{
		flSteerEdit = &flSteerMax;
	}

	if( bSliding )
	{
		*flSteerEdit *= -0.25f;
	}
	flCurrentSteer = clamp( flCurrentSteer, flSteerMin, flSteerMax );

	GetKart()->SetSteering( flCurrentSteer );
}


void C_MKSEngine::Jump( CUserCmd *pCmd )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart ) return;

	if( (m_iButtons & IN_JUMP) == 0 ) //Not Jumping
	{
		mksKart->SetSliding( false );
		m_iSlideDirection = 0;
		mksKart->SetWheelFriction( mks_friction_default.GetFloat() );
		m_iOldButtons &= ~IN_JUMP;
		return;
	}
	
	if( (m_iOldButtons & IN_JUMP) > 0 ) //Holding Jump (after jumping)
	{
		Slide( pCmd );
		return;
	}

	

	C_MKSPlayer *mksPlayer = mksKart->GetDriver();
	if( !mksPlayer ) return;

 	IPhysicsObject *pKart = mksKart->VPhysicsGetObject();
	if( !pKart ) return;

	if( mksKart->GetWheelGroundCount() < 4 )
		return;

	if( pCmd->sidemove != 0.0f )
	{
		if( pCmd->sidemove < 0.0f )
			m_iSlideDirection = -1;
		else
			m_iSlideDirection = 1;
	}

	Vector vJumpVelocity = Vector(0,0, mks_jump_speed.GetFloat() );
	pKart->ApplyForceCenter( vJumpVelocity );
	
	m_iOldButtons |= IN_JUMP;
}


void C_MKSEngine::Slide( CUserCmd *pCmd )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart ) return;
	
//	float flSideMove = pCmd->sidemove;
	float flFriction = mks_friction_slide.GetFloat();
	mksKart->SetWheelFriction( flFriction );
	mksKart->SetSliding( true );
}


