#include "cbase.h"

#include "movevars_shared.h"

#include "mks/Kart/c_mks_kartmovement.h"
#include "mks/Rules/mks_cvar_settings.h"
#include "mks/Kart/c_mks_clientkart.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_wheel.h"
#include "mks/Math/mks_math.h"

C_MKSKartMovement::C_MKSKartMovement()
{
	m_bActive = false;
	m_flFrametime = 0.0f;

	m_vFinalVelocity = Vector(0,0,0);
}

C_MKSKartMovement::~C_MKSKartMovement()
{
	if ( m_pController )
	{
		physenv->DestroyMotionController( m_pController );
		m_pController = NULL;
	}
}


//-----------------------------------------------------
// C_MKSKartMovement::CreateKartMovement
//
// param1: Physics Kart to manipulate
// param2: Player driving
//
// returns: KartMovement object
//
// Description: Setup KartMovement object with motion controller and save the pointers to the driver and kart objects.
//-----------------------------------------------------
C_MKSKartMovement *C_MKSKartMovement::CreateKartMovement( C_MKSClientKart *pKart )
{
	C_MKSKartMovement *pKartMovement = new C_MKSKartMovement();

	if ( !pKartMovement )
		return NULL;

#ifdef DEBUG
	Warning( "Created - \"mks_kartmovement\" \n"  );
#endif

	pKartMovement->SetKart( pKart );

	IPhysicsObject *pPhysicsKart = pKart->VPhysicsGetObject();

	pKartMovement->m_pController = physenv->CreateMotionController( (IMotionEvent *)pKartMovement );
	pKartMovement->m_pController->AttachObject( pPhysicsKart, false );

	pKartMovement->m_bActive = true;

	return pKartMovement;
}


//-----------------------------------------------------
// C_MKSKartMovement::Simulate
//
// param1: Motion Controller for movement
// param2: Physics Object
// param3: Time Between frames
// param4: Velocity to be added this frame.
// param5: Angular Veloctiy to be added this frame.
//
// returns: simresult, how to manipulate Velocity and Angular Velocity.
//
// Description: Motion Controller's simulation callback for manipulating the movement of our physics object.
//-----------------------------------------------------
IMotionEvent::simresult_e C_MKSKartMovement::Simulate( IPhysicsMotionController *pController, IPhysicsObject *pObject, float deltaTime, Vector &linear, AngularImpulse &angular )
{
	if ( !m_bActive )
		return SIM_NOTHING;

	pObject->Wake();
	float invDeltaTime = (1/deltaTime);

	m_flFrametime = deltaTime;

	//Process kart's movement.
	C_MKSClientKart *mksKart = GetKart();

	if( !mksKart )
		return SIM_NOTHING;

	mksKart->Update();
	
	mksKart->Drive();

	CalculateAngularVelocity();
	CalculateVelocity();


	linear = m_vFinalVelocity * invDeltaTime;
	m_vFinalVelocity = Vector(0,0,0);

//Draw lines for debugging
#if DEBUG
	DrawDebugLines();
#endif

	return SIM_GLOBAL_ACCELERATION;
}


//-----------------------------------------------------
// C_MKSKartMovement::CalculateGravity
//
//
// Description: Simulate gravity to be added to the velocity.
//-----------------------------------------------------
void C_MKSKartMovement::CalculateGravity( void )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart )
		return;

	IPhysicsObject *physKart = mksKart->VPhysicsGetObject();
	if( !physKart )
		return;

	Vector vVelocity;
	AngularImpulse angImpulse;
	physKart->GetVelocity( &vVelocity, &angImpulse );

	physKart->EnableGravity( true );

	Vector vGravity = mksKart->GetGravityDirection();
	float flGravity = mksKart->GetGravity() * sv_gravity.GetFloat();

	vVelocity -= Vector(0,0,-1) * flGravity * gpGlobals->frametime;

	
	physKart->SetVelocityInstantaneous( &vVelocity, &angImpulse );

	//ClipVelocity( vVelocity, -vGravity, vVelocity, 1 );

	vGravity = vGravity * flGravity * gpGlobals->frametime;
	//physKart->ApplyForceCenter( vGravity );
	m_vFinalVelocity += vGravity;
}

//-----------------------------------------------------
// C_MKSKartMovement::CalculateVelocity
//
//
// Description: Gather the kart and wheel velocities, and combine them.
//-----------------------------------------------------
void C_MKSKartMovement::CalculateVelocity( void )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart )
		return;

	IPhysicsObject *physKart = mksKart->VPhysicsGetObject();
	if( !physKart )
		return;

	//Add velocity from external sources.
	m_vFinalVelocity += mksKart->GetAddVelocity();

	CalculateGravity();

	mksKart->ClearAddVelocity();

	Vector vVelocity = Vector(0,0,0);
	AngularImpulse angImpulse = mksKart->GetAngularVelocity();

	physKart->AddVelocity( &vVelocity, &angImpulse );
}


//-----------------------------------------------------
// C_MKSKartMovement::CalculateAngularVelocity
//
//
// Description: Calculate the angular velocity to keep the kart upright
//-----------------------------------------------------
void C_MKSKartMovement::CalculateAngularVelocity( void )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart ) 
		return;

	C_MKSPlayer *mksPlayer = GetKart()->GetDriver();
	if( !mksPlayer )
		return;

	IPhysicsObject *physKart = mksKart->VPhysicsGetObject();
	if(!physKart)
		return;

	matrix3x4_t matrix;
	physKart->GetPositionMatrix( &matrix );

	// Get the alignment axis in object space
	Vector vGravOpposite = -mksKart->GetGravityDirection();
	Vector currentLocalTargetAxis;
	VectorIRotate( vGravOpposite, matrix, currentLocalTargetAxis );

	float dotprod = DotProduct( currentLocalTargetAxis, vGravOpposite );
	if(dotprod > 1.0f)
		dotprod = 1.0f;
	
	float angle = RAD2DEG(acos(dotprod));
	
	AngularImpulse angVelocity;
	Vector vVelocity, vWheelVelocity;
	physKart->GetVelocity( &vVelocity, &angVelocity );
	
	float flAngularLimit = mks_upright_ground.GetFloat();
	float flInvDeltaTime = 1 / m_flFrametime;
	float angPerSec = angle * flAngularLimit;

	if( angle > 50.0f )
		angPerSec = angle * 5000.0f;

	angVelocity += MKSComputeRotSpeedToAlignAxes( vGravOpposite, 
												currentLocalTargetAxis, 
												angVelocity, 
												1.0f, 
												flInvDeltaTime * flInvDeltaTime, 
												angPerSec )  * m_flFrametime;

	mksKart->SetAngularVelocity( angVelocity );
}


//-----------------------------------------------------
// MKSComputeRotSpeedToAlignAxes
//
// param1: Local Start Axis
// param2: Local End Axis
// param3: Current Angle Velocity
// param4: Damping (Smooth orientation)
// param5: Delta Time
// param6: Angles Per Second
//
// returns: Angular Velocity
//
// Description: Computes the torque necessary to align testAxis with alignAxis
//-----------------------------------------------------
AngularImpulse MKSComputeRotSpeedToAlignAxes( const Vector &testAxis, const Vector &alignAxis, const AngularImpulse &currentSpeed, float damping, float scale, float maxSpeed )
{
	Vector rotationAxis = CrossProduct( testAxis, alignAxis );
	
	// atan2() is well defined, so do a Dot & Cross instead of asin(Cross)
	float cosine = DotProduct( testAxis, alignAxis );

	if(cosine < -0.7)
		rotationAxis = CrossProduct( testAxis, Vector(0,1,0) );

	//if(maxSpeed == 80000.0f)
	//	rotationAxis = Vector(0,0,1);//CrossProduct( testAxis, Vector(1,1,0) );
	//maxSpeed *= 2 * (1.0f-cosine);
	float sine = VectorNormalize( rotationAxis );
	float angle = atan2( sine, cosine );

	angle = RAD2DEG(angle);
	AngularImpulse angular = rotationAxis * scale * angle;
	angular -= rotationAxis * damping * DotProduct( currentSpeed, rotationAxis );

	float len = VectorNormalize( angular );

	if ( len > maxSpeed )
	{
		len = maxSpeed;
	}

	return angular * len;
}

//-----------------------------------------------------
// C_MKSKartMovement::GetDriver
//
// returns: Player Driver
//
// Description: Shortcut for getting the player driver.
//-----------------------------------------------------
C_MKSPlayer *C_MKSKartMovement::GetDriver( void ) 
{ 
	return m_pKart->GetDriver(); 
};


//-----------------------------------------------------
// C_MKSKartMovement::DrawDebugLines
//
// Description: Draw the physics cube, tire forces and driving force using lines.
//-----------------------------------------------------
void C_MKSKartMovement::DrawDebugLines( void )
{
	C_MKSClientKart *mksKart = GetKart();
	//IPhysicsObject *pKart = mksKart->VPhysicsGetObject();

	Vector top_fl, top_fr, top_rr, top_rl;
	Vector bottom_fl, bottom_fr, bottom_rr, bottom_rl;
	Vector position, vForward, vRight, vUp;
	QAngle angles;

	Vector mins = mksKart->m_vCubeMins;
	Vector maxs = mksKart->m_vCubeMaxs;

	position = mksKart->GetWorldOrigin();
	vForward = mksKart->GetForward();
	vRight = mksKart->GetRight();
	vUp = mksKart->GetUp();

	//pKart->GetPosition( &position, &angles );

	//Convert QAngle to 3 different Vectors
	//AngleVectors( angles, &vForward, &vRight, &vUp );

	//Set all to our position.
	top_fl = top_fr = top_rl = top_rr = bottom_fl = bottom_fr = bottom_rl = bottom_rr = position;

	//Rotate based on the current angles.
	top_fl += (vForward * maxs.x) + (vRight * maxs.y) + (vUp * maxs.z);
	top_fr += (vForward * mins.x) + (vRight * maxs.y) + (vUp * maxs.z);
	top_rl += (vForward * maxs.x) + (vRight * mins.y) + (vUp * maxs.z);
	top_rr += (vForward * mins.x) + (vRight * mins.y) + (vUp * maxs.z);
	bottom_fl += (vForward * maxs.x) + (vRight * maxs.y) + (vUp * mins.z);
	bottom_fr += (vForward * mins.x) + (vRight * maxs.y) + (vUp * mins.z);
	bottom_rl += (vForward * maxs.x) + (vRight * mins.y) + (vUp * mins.z);
	bottom_rr += (vForward * mins.x) + (vRight * mins.y) + (vUp * mins.z);

	/*
	//Top 4 Horizonal Lines
	NDebugOverlay::Line( top_fl, top_fr, 255, 0, 0, 0, 0 );
	NDebugOverlay::Line( top_rl, top_rr, 255, 0, 255, 0, 0 );
	NDebugOverlay::Line( top_fl, top_rl, 0, 255, 0, 0, 0 );
	NDebugOverlay::Line( top_fr, top_rr, 0, 0, 255, 0, 0 );

	//Bottom 4 Horizontal Lines
	NDebugOverlay::Line( bottom_fl, bottom_fr, 255, 0, 0, 0, 0 );
	NDebugOverlay::Line( bottom_rl, bottom_rr, 255, 0, 255, 0, 0 );
	NDebugOverlay::Line( bottom_fl, bottom_rl, 0, 255, 0, 0, 0 );
	NDebugOverlay::Line( bottom_fr, bottom_rr, 0, 0, 255, 0, 0 );

	//Middle 4 Vertical Lines
	NDebugOverlay::Line( top_fl, bottom_fl, 255, 0, 0, 0, 0 );
	NDebugOverlay::Line( top_fr, bottom_fr, 255, 0, 0, 0, 0 );
	NDebugOverlay::Line( top_rl, bottom_rl, 255, 0, 0, 0, 0 );
	NDebugOverlay::Line( top_rr, bottom_rr, 255, 0, 0, 0, 0 );

	//Line Straight up from the origin
	NDebugOverlay::Line( position, position + Vector(0,0,1) * 25.0, 255, 0, 0, 0, 0 );

	CMKSWheel *pWheel = NULL;
	int iWheelCount = mksKart->WheelCount();
	int i;
	for(i=0; i<iWheelCount; i++)
	{
		pWheel = mksKart->GetWheel(i);
		if(!pWheel)
			continue;
	
		Vector vStart = pWheel->GetWorldOrigin();
		Vector vEnd = vStart + pWheel->GetDriveForce();
		NDebugOverlay::Line( vStart, vEnd, 255, 255, 255, 0, 0 );

		vEnd = vStart + pWheel->GetFrictionForce();
		NDebugOverlay::Line( vStart, vEnd, 0, 255, 0, 0, 0 );

		vEnd = vStart + pWheel->GetSuspensionForce();
		NDebugOverlay::Line( vStart, vEnd, 255, 0, 0, 0, 0 );
	}
	*/
}