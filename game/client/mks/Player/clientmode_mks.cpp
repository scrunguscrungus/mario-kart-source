#include "cbase.h"

#include "view_shared.h"

#include "mks/camera/mks_camera.h"
#include "mks/player/clientmode_mks.h"
#include "mks/player/c_mks_player.h"
#include "mks/kart/mks_kart_profile.h"

void ClientModeMKSNormal::OverrideView( CViewSetup *pSetup )
{
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if(!pPlayer)
		return;
	
	C_MKSPlayer *mksPlayer = ToMKSPlayer( pPlayer );
	if(!mksPlayer)
		return;
	
	MKSCamera *pCamera = mksPlayer->GetCamera();
	if(!pCamera)
		return;
	
	if( !pCamera->GetTarget() ) {
		//pSetup->origin = mksPlayer->GetAbsOrigin() + Vector(0,0,72);
		//pSetup->angles = mksPlayer->GetAbsAngles();

		BaseClass::OverrideView( pSetup );	
		return;
	}
	mksPlayer->GetCamera()->UpdateCamera( false );
	mksPlayer->GetCamera()->GetCameraInfo( pSetup->origin, pSetup->angles );

	engine->SetViewAngles( pSetup->angles );
}

ClientModeMKSNormal* GetClientModeMKSNormal()
{
	Assert( dynamic_cast< ClientModeMKSNormal* >( GetClientModeNormal() ) );

	return static_cast< ClientModeMKSNormal* >( GetClientModeNormal() );
}

void ClientModeMKSNormal::Init( void )
{
	BaseClass::Init();

	if( g_KartProfiles == NULL )
	{
		g_KartProfiles = new MKSKartProfile();
		if( g_KartProfiles )
		{
			g_KartProfiles->LoadProfiles();
		}
	}
}