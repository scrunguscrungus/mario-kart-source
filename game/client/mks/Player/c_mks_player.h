//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef C_MKS_PLAYER_H
#define C_MKS_PLAYER_H
#ifdef _WIN32
#pragma once
#endif

#include "c_sdk_player.h"

class MKSCamera;
class C_MKSKart;
class C_MKSClientKart;

class C_MKSPlayer : public C_SDKPlayer
{
public:
	DECLARE_CLASS( C_MKSPlayer, C_SDKPlayer );
	DECLARE_CLIENTCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_INTERPOLATION();

	C_MKSPlayer();
	~C_MKSPlayer();

	virtual void LocalPlayerRespawn( void );

	//virtual void PostDataUpdate( DataUpdateType_t updateType );
	virtual void OnDataChanged( DataUpdateType_t updateType );

	virtual void CalcView( Vector &eyeOrigin, QAngle &eyeAngles, float &zNear, float &zFar, float &fov );
	virtual void ClientThink( void );

	virtual void InitClientKart( void );

public:
	C_MKSClientKart *GetClientKart() { return m_pClientKart; };
	void SetClientKart( C_MKSClientKart *pKart ) { m_pClientKart = pKart; };

	C_MKSKart *GetServerKart() { return m_pServerKart; };
	void SetServerKart( C_MKSKart *pKart ) { m_pServerKart = pKart; };

	MKSCamera *GetCamera() { return m_pCamera; };
	void SetCamera( MKSCamera *pCamera ) { m_pCamera = pCamera; };

	const char *GetKartName() { return "Default"; };
	void SetKartName( const char *szKartName ) { m_szKartName = (char *)szKartName; };

	CUserCmd *GetCurrentUserCmd() { return &m_pCurrentCmd; };
	void SetCurrentUserCmd( CUserCmd *pCmd ) { m_pCurrentCmd = *pCmd; };

private:

	char *m_szKartName;
	C_MKSClientKart *m_pClientKart;
	C_MKSKart *m_pServerKart;

	MKSCamera *m_pCamera;

	CUserCmd m_pCurrentCmd;

	bool m_bHasItem;

	EHANDLE m_hCurrentItem;

	//C_MKSPlayer( const C_MKSPlayer & );
};


inline C_MKSPlayer* ToMKSPlayer( CBaseEntity *pPlayer )
{
	if ( !pPlayer || !pPlayer->IsPlayer() )
		return NULL;

	return static_cast< C_MKSPlayer* >( pPlayer );
}

#endif // C_MKS_PLAYER_H
