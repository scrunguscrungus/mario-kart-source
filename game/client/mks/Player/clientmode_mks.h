//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef MKS_CLIENTMODE_H
#define MKS_CLIENTMODE_H
#ifdef _WIN32
#pragma once
#endif

#include "clientmode_sdk.h"
#include "sdkviewport.h"

class ClientModeMKSNormal : public ClientModeSDKNormal 
{
DECLARE_CLASS( ClientModeMKSNormal, ClientModeSDKNormal );

private:

// IClientMode overrides.
public:

	//				ClientModeMKSNormal();
	//virtual			~ClientModeMKSNormal();

	virtual void	OverrideView( CViewSetup *pSetup );

	virtual void	Init();
	//virtual void	InitViewport();

	//virtual float	GetViewModelFOV( void );

	//int				GetDeathMessageStartHeight( void );

	//virtual void	PostRenderVGui();

	//virtual bool	CanRecordDemo( char *errorMsg, int length ) const;

private:
	
	//	void	UpdateSpectatorMode( void );

};


extern ClientModeMKSNormal* GetClientModeMKSNormal();


#endif // MKS_CLIENTMODE_H
