
extern ConVar mks_spring_stiff;
extern ConVar mks_spring_damping;

extern ConVar mks_friction_default;
extern ConVar mks_friction_slide;
extern ConVar mks_friction_multiplier;

extern ConVar mks_upright_ground;

extern ConVar mks_forward_speed;
extern ConVar mks_forward_maxspeed;
extern ConVar mks_reverse_speed;
extern ConVar mks_reverse_maxspeed;
extern ConVar mks_neutral_decayspeed;



extern ConVar mks_jump_speed;

extern ConVar mks_steering_min;
extern ConVar mks_steering_max;
extern ConVar mks_steering_speed;

extern ConVar mks_wall_bounce;

/*
extern ConVar mks_wheel_radius;

extern ConVar mks_wheel_rotDrag;
extern ConVar mks_wheel_mass;
extern ConVar mks_wheel_rotDamp;
extern ConVar mks_wheel_rotIner;
extern ConVar mks_wheel_inertiax;
extern ConVar mks_wheel_inertiay;
extern ConVar mks_wheel_inertiaz;
extern ConVar mks_wheel_forward_dist;
extern ConVar mks_wheel_side_dist;
extern ConVar mks_wheel_up_dist;
extern ConVar mks_wheel_scale;
extern ConVar mks_wheel_rightoffset;
extern ConVar mks_wheel_forwardoffset;
extern ConVar mks_wheel_elastic;

extern ConVar mks_move_forward;
extern ConVar mks_move_side;
extern ConVar mks_move_turn;
extern ConVar mks_move_maxreverse;
extern ConVar mks_move_maxforward;
extern ConVar mks_move_airtime;
extern ConVar mks_move_reverse;

extern ConVar mks_body_damping;
extern ConVar mks_body_dragCo;
extern ConVar mks_body_rotDrag;
extern ConVar mks_body_mass;
extern ConVar mks_body_rotDamp;
extern ConVar mks_body_rotIner;
extern ConVar mks_body_inertiax;
extern ConVar mks_body_inertiay;
extern ConVar mks_body_inertiaz;


extern ConVar mks_jump_delay;
extern ConVar mks_jump_speed;
extern ConVar mks_jump_max;

extern ConVar mks_lerp_speed;
extern ConVar mks_spawndist;
extern ConVar mks_origin_maxdist;

extern ConVar mks_bounce;

extern ConVar mks_traction_converge;


extern ConVar mks_upright_air_max;
extern ConVar mks_upright_air_min;
extern ConVar mks_upright_sticky;
extern ConVar mks_upright_floating;

extern ConVar mks_gravity_air_speed;
extern ConVar mks_gravity_air_max;
extern ConVar mks_gravity_ground_max;
extern ConVar mks_gravity_ground_speed;
extern ConVar mks_gravity_default_speed;
extern ConVar mks_gravity_default_max;


extern ConVar mks_bounceSpring_stiff;
extern ConVar mks_bounceSpring_damp;

extern ConVar mks_prediction_distance_error;

extern ConVar mks_fakelag;
//extern ConVar mks_player_size;
*/