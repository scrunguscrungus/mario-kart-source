#include "cbase.h"
#ifdef GAME_DLL
#include "mks/Entities/mks_shell.h"
#include "mks/Player/mks_player.h"
#include "mks/Kart/mks_kart.h"
#endif
#include "mks_item.h"

class CMKSItemGreenShell : public CMKSItem
{
public:
	DECLARE_CLASS( CMKSItemGreenShell, CMKSItem );
	DECLARE_NETWORKCLASS();
	CMKSItemGreenShell();
	virtual MKSItemID GetItemId() {return ITEM_GREEN_SHELL;}
	virtual void AttackDown();
	virtual void AttackUp();
	virtual bool IsItemDone() {return m_bHasShot;}
private:
#ifdef GAME_DLL
	Shell *m_pShell;

#endif
	bool m_bHasShot;
};

LINK_ENTITY_TO_CLASS( mks_item_green_shell, CMKSItemGreenShell );

IMPLEMENT_NETWORKCLASS_DT( CMKSItemGreenShell, DT_MKSItemGreenShell )
END_NETWORK_TABLE()

CMKSItemGreenShell::CMKSItemGreenShell()
{
#ifdef GAME_DLL
	m_bHasShot = false;
	m_pShell = 0;
#endif
}

void CMKSItemGreenShell::AttackDown()
{
#ifdef GAME_DLL
	m_pShell = Shell::CreateShell(SHELL_GREEN, m_pOwner->GetAbsOrigin(), m_pOwner->GetAbsAngles(), m_pOwner);
	m_pShell->Spawn();
	m_pShell->FollowEntity(m_pOwner->GetKart());
	//m_pShell->StartSpin();
#endif
}

void CMKSItemGreenShell::AttackUp()
{
#ifdef GAME_DLL
	m_pShell->Fire(m_pOwner->GetKart()->GetAbsAngles(), 3000);
	m_bHasShot = true;
#endif
}
