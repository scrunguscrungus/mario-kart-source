#include "cbase.h"
#include "filesystem.h"
#include "gamestringpool.h"

#ifdef CLIENT_DLL
	#include "mks/Player/c_mks_player.h"
	#include "mks/Kart/c_mks_kart.h"
#else
	#include "mks/Player/mks_player.h"
	#include "mks/Kart/mks_kart.h"
#endif

//Singleton Kart Profile
MKSKartProfile *g_KartProfiles = NULL;

#ifdef GAME_DLL
void SendProxy_MKSString_tToString( const SendProp *pProp, const void *pStruct, const void *pData, DVariant *pOut, int iElement, int objectID )
{
	string_t *pString = (string_t*)pData;
	pOut->m_pString = (char*)STRING( *pString );
}
#endif


BEGIN_NETWORK_TABLE_NOBASE( MKSPlayerProfile, DT_MKSKartProfileInfo )
#ifdef CLIENT_DLL
	RecvPropInt( RECVINFO( id ) ),
	RecvPropString( RECVINFO( szName ) ),
	RecvPropString( RECVINFO( szModel ) ),

	RecvPropFloat( RECVINFO( flDamp ) ),
	RecvPropFloat( RECVINFO( flDragCo ) ),
	RecvPropFloat( RECVINFO( flMass ) ),
	RecvPropFloat( RECVINFO( flRotDamp ) ),
	RecvPropFloat( RECVINFO( flRotDrag ) ),
	RecvPropFloat( RECVINFO( flRotInertia ) ),

	RecvPropFloat( RECVINFO( vInertiaX ) ),
	RecvPropFloat( RECVINFO( vInertiaY ) ),
	RecvPropFloat( RECVINFO( vInertiaZ ) ),

	RecvPropString( RECVINFO( szFrontLeft ) ),
	RecvPropString( RECVINFO( szRearRight ) ),

	RecvPropInt( RECVINFO( iWheelCount ) ),

	RecvPropArray( RecvPropInt( RECVINFO( iWheelAttachment[0] ) ), iWheelAttachment ),
	RecvPropArray( RecvPropFloat( RECVINFO( flWheelRadius[0] ) ), flWheelRadius ),
	RecvPropArray( RecvPropFloat( RECVINFO( flWheelWidth[0] ) ), flWheelWidth ),
	RecvPropArray( RecvPropInt( RECVINFO( iWheelSpeed[0] ) ), iWheelSpeed ),
	RecvPropArray( RecvPropInt( RECVINFO( iWheelSteer[0] ) ), iWheelSteer ),
#else
	SendPropInt( SENDINFO( id ), 8 ),
	SendPropString( SENDINFO( szName ) ),
	SendPropString( SENDINFO( szModel ) ),

	SendPropFloat( SENDINFO( flDamp ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( flDragCo ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( flMass ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( flRotDamp ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( flRotDrag ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( flRotInertia ), 0, SPROP_NOSCALE ),

	SendPropFloat( SENDINFO( vInertiaX ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( vInertiaY ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( vInertiaZ ), 0, SPROP_NOSCALE ),

	SendPropString( SENDINFO( szFrontLeft ) ),
	SendPropString( SENDINFO( szRearRight ) ),

	SendPropInt( SENDINFO( iWheelCount ) ),

	SendPropArray( SendPropInt( SENDINFO_ARRAY( iWheelAttachment ), 16 ), iWheelAttachment ),
	SendPropArray( SendPropFloat( SENDINFO_ARRAY( flWheelRadius ), 0, SPROP_NOSCALE ), flWheelRadius ),
	SendPropArray( SendPropFloat( SENDINFO_ARRAY( flWheelWidth ), 0, SPROP_NOSCALE ), flWheelWidth ),
	SendPropArray( SendPropInt( SENDINFO_ARRAY( iWheelSpeed ), 8 ), iWheelSpeed ),
	SendPropArray( SendPropInt( SENDINFO_ARRAY( iWheelSteer ) ), iWheelSteer ),
#endif
END_NETWORK_TABLE()


MKSKartProfile::MKSKartProfile()
{
	
}

MKSKartProfile::~MKSKartProfile()
{
	m_Profiles.PurgeAndDeleteElements();
}


//-----------------------------------------------------
// MKSKartProfile::GetProfile
//
// param1: Name of kart profile 
//
// returns: MKSPlayerProfile
//
// Description: Using the profile name, retrieves the saved profile object.
//-----------------------------------------------------
MKSPlayerProfile *MKSKartProfile::GetProfile( const char *szName )
{
	MKSPlayerProfile *profile;
	int count = m_Profiles.Count();
	int i;
	for(i=0; i<count; i++)
	{
		profile = m_Profiles[i];
		if( profile == NULL )
			continue;
		if( Q_strcmp( szName, profile->szName ) == 0 )
		{
			return profile;
		}
	}
	return NULL;
}
#ifdef GAME_DLL
CON_COMMAND( mks_reloadprofiles, "Reload all Profiles" )
{
	if( g_KartProfiles )
	{
		g_KartProfiles->ReloadProfiles();

		CBasePlayer *pPlayer = ToBasePlayer( UTIL_GetCommandClient() ); 
		if( !pPlayer )
			return;

		CMKSPlayer *mksPlayer = ToMKSPlayer( pPlayer );
		if( !mksPlayer )
			return;

		CMKSKart *mksKart = mksPlayer->GetKart();
		if( !mksKart )
			return;

		IPhysicsObject *pKart = mksKart->VPhysicsGetObject();
		if( !pKart )
			return;

		Vector vOrigin;
		QAngle aAngle;
		Vector vVelocity;
		AngularImpulse angVelocity;

		pKart->GetPosition( &vOrigin, &aAngle );
		pKart->GetVelocity( &vVelocity, &angVelocity );

		mksPlayer->SetParent( NULL );
		mksPlayer->SetOwnerEntity( NULL );

		mksKart->Remove();
		mksKart = CMKSKart::CreateKart( mksPlayer );
		mksPlayer->SetKart( mksKart );
		pKart = mksKart->VPhysicsGetObject();

		pKart->SetPosition( vOrigin, aAngle, true );
		pKart->SetVelocityInstantaneous( &vVelocity, &angVelocity );


	}
}
#endif

void MKSKartProfile::ReloadProfiles()
{
	m_Profiles.RemoveAll();

	LoadProfiles();
/*
	CBasePlayer *pPlayer = NULL;
	CMKSPlayer *mksPlayer = NULL;
	int id;
	for( id=1; id<=gpGlobals->maxClients; id++ )
	{
		pPlayer = UTIL_PlayerByIndex( id );
		if( !pPlayer )
			continue;

		mksPlayer = ToMKSPlayer( pPlayer );
		if( !mksPlayer )
			continue;

#ifdef GAME_DLL
		
#endif
	}
	*/
}

//-----------------------------------------------------
// MKSKartProfile::LoadProfiles
//
// Description: Load all the kart script files located at "scripts/mks/kart"
//-----------------------------------------------------
void MKSKartProfile::LoadProfiles()
{
	FileFindHandle_t findHandle;

	//Begin an iterative search of kart profiles
	const char *fileName = filesystem->FindFirstEx( "scripts/mks/kart/*.txt", "MOD", &findHandle );
	char fullPath[256];

	while ( fileName )
	{
		//Our files will always have "KartData" as the parent
		KeyValues *kProfile = new KeyValues( "KartData" );

		if( !kProfile )
		{
			Warning( "Kart Profile is not a valid KeyValue file: %s", fileName );
			continue;
		}

		Q_snprintf( fullPath, sizeof( fullPath ), "scripts/mks/kart/%s", fileName);

		Msg( "%s \n", fullPath );
		//Load the file into KeyValues
		kProfile->LoadFromFile( filesystem, fullPath, "GAME" );
		
		//Parse the KeyValue data
		ParseProfile( kProfile );
		
		//Delete KeyValue object from memory
		kProfile->deleteThis();


		//Get the next .txt file in the directory
		fileName = filesystem->FindNext( findHandle );
	}

	//Close the filesystem search
	filesystem->FindClose( findHandle );
}

//-----------------------------------------------------
// MKSKartProfile::ParseProfile
//
// param1: KeyValue object of current script file.
//
// Description: Parse all the profile values and save them to our Kart Profile list.
//-----------------------------------------------------
void MKSKartProfile::ParseProfile( KeyValues *kProfile )
{
	MKSPlayerProfile *profile = new MKSPlayerProfile;


	Q_strncpy( profile->szName.GetForModify(), kProfile->GetString( "name", "!!! Missing Kart Name" ), MKS_MAX_STRING_NAME );
	Q_strncpy( profile->szModel.GetForModify(), kProfile->GetString( "model", "!!! Missing Kart Model" ), MKS_MAX_STRING_NAME );

	

	profile->flMass = kProfile->GetFloat( "mass", 5.0 );

	profile->flDamp = kProfile->GetFloat( "damping", 0.01 );
	profile->flDragCo = kProfile->GetFloat( "dragco", 100.0 );
	profile->flRotDrag = kProfile->GetFloat( "rotdrag", 100.0 );
	profile->flRotDamp = kProfile->GetFloat( "rotdamp", 50.0 );
	profile->flRotInertia = kProfile->GetFloat( "rotinertia", 0.2 );

	profile->vInertiaX = kProfile->GetFloat( "intertia_x", 3.0 );
	profile->vInertiaY = kProfile->GetFloat( "intertia_y", 4.0 );
	profile->vInertiaZ = kProfile->GetFloat( "intertia_z", 1.0 );
	
	Q_strncpy( profile->szFrontLeft.GetForModify(), kProfile->GetString( "front_left", "!!! Missing front_left Attachment Name" ), MKS_MAX_STRING_NAME );
	Q_strncpy( profile->szRearRight.GetForModify(), kProfile->GetString( "rear_right", "!!! Missing rear_right Attachment Name" ), MKS_MAX_STRING_NAME );

	KeyValues *kWheel = kProfile->FindKey( "wheel" );
	//KeyValues *pTemp = kProfile->GetFirstSubKey();

	int a;
	int iWheelCount = 0;

	for( a=0; a<MKS_MAX_WHEELS; a++ )
	{
		if( kWheel )
		{
			Q_strncpy( profile->szWheelAttachment[iWheelCount], kWheel->GetString( "attachment", "!!! Missing Attachment name" ), MKS_MAX_STRING_NAME );
			Q_strncpy( profile->szWheelSuspension[iWheelCount], kWheel->GetString( "suspension", "!!! Missing PoseParameter name" ), MKS_MAX_STRING_NAME );
		
			profile->iWheelAttachment.Set( iWheelCount, -1 );
			profile->iWheelSuspension.Set( iWheelCount, -1 );

			profile->flWheelRadius.Set( iWheelCount, kWheel->GetFloat( "radius", 0.01 ) );
			profile->flWheelWidth.Set( iWheelCount, kWheel->GetFloat( "width", 3.0 ) );
			profile->iWheelSpeed.Set( iWheelCount, kWheel->GetInt( "speed", 3 ) );
			profile->iWheelSteer.Set( iWheelCount, kWheel->GetInt( "steering", 1 ) );
			kWheel = kWheel->GetNextKey();
			iWheelCount++;
		}
		else
		{
			profile->iWheelAttachment.Set( a, -1 );
			profile->iWheelSuspension.Set( a, -1 );
			profile->flWheelRadius.Set( a, 0.0 );
			profile->flWheelWidth.Set( a, 0.0 );
			profile->iWheelSpeed.Set( a, 0 );
			profile->iWheelSteer.Set( a, 0 );
		}

		
	}

	profile->iWheelCount = iWheelCount;
	
	profile->id = m_Profiles.AddToTail( profile );

}


//-----------------------------------------------------
// MKSPlayerProfile::Copy
//
// param1: Profile to copy from
//
// Description: Deep Copy all the data from another profile.
//-----------------------------------------------------
void MKSPlayerProfile::Copy( MKSPlayerProfile *fromProfile )
{
	Q_strncpy( szName.GetForModify(), fromProfile->szName, MKS_MAX_STRING_NAME );
	Q_strncpy( szModel.GetForModify(), fromProfile->szModel, MKS_MAX_STRING_NAME );
	

	flDamp = fromProfile->flDamp;
	flDragCo = fromProfile->flDragCo;
	flMass = fromProfile->flMass;
	flRotDamp = fromProfile->flRotDamp;
	flRotDrag = fromProfile->flRotDrag;
	flRotInertia = fromProfile->flRotInertia;
	
	vInertiaX = fromProfile->vInertiaX;
	vInertiaY = fromProfile->vInertiaY;
	vInertiaZ = fromProfile->vInertiaZ;

	Q_strncpy( szFrontLeft.GetForModify(), fromProfile->szFrontLeft, MKS_MAX_STRING_NAME );
	Q_strncpy( szRearRight.GetForModify(), fromProfile->szRearRight, MKS_MAX_STRING_NAME );

	iWheelCount = fromProfile->iWheelCount;

	int a;
	for( a=0; a<iWheelCount; a++ )
	{

		//char *szAttach = "";
		//Q_strncpy( iWheelAttachment.GetForModify(a), fromProfile->iWheelAttachment.Get(a), MKS_MAX_STRING_NAME );
		//szAttachment.Set( a, fromProfile->szAttachment.GetForModify(a) );
		//string_t szAttach = fromProfile->szAttachment;
		Q_strncpy( szWheelAttachment[a], fromProfile->szWheelAttachment[a], MKS_MAX_STRING_NAME );

		Q_strncpy( szWheelSuspension[a], fromProfile->szWheelSuspension[a], MKS_MAX_STRING_NAME );

		iWheelSuspension.Set( a, fromProfile->iWheelSuspension[a] );
		iWheelAttachment.Set(a, fromProfile->iWheelAttachment[a] );
		iWheelSteer.Set(a, fromProfile->iWheelSteer[a] );
		iWheelSpeed.Set(a, fromProfile->iWheelSpeed[a] );
		flWheelRadius.Set(a, fromProfile->flWheelRadius[a] );
		flWheelWidth.Set(a, fromProfile->flWheelWidth[a] );
	}
}