//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "gameinterface.h"
#include "mapentities.h"

#include "mks/Kart/mks_kart_profile.h"
#include "mks/Player/mks_player.h"

// -------------------------------------------------------------------------------------------- //
// Mod-specific CServerGameClients implementation.
// -------------------------------------------------------------------------------------------- //
void CServerGameClients::GetPlayerLimits( int& minplayers, int& maxplayers, int &defaultMaxPlayers ) const
{
	minplayers = 2;  // Force multiplayer.
	maxplayers = MAX_PLAYERS;
	defaultMaxPlayers = 32;
}

// -------------------------------------------------------------------------------------------- //
// Mod-specific CServerGameDLL implementation.
// -------------------------------------------------------------------------------------------- //
void CServerGameDLL::LevelInit_ParseAllEntities( const char *pMapEntities )
{
	//MKS - Load Kart Profiles
	if( g_KartProfiles == NULL )
	{
		g_KartProfiles = new MKSKartProfile();
		if( g_KartProfiles )
		{
			g_KartProfiles->LoadProfiles();
		}
	}
}

// -------------------------------------------------------------------------------------------- //
// Mod-specific CServerGameDLL Think implementation.
// -------------------------------------------------------------------------------------------- //
void CServerGameDLL::Think( bool finalTick )
{
	if ( m_fAutoSaveDangerousTime != 0.0f && m_fAutoSaveDangerousTime < gpGlobals->curtime )
	{
		// The safety timer for a dangerous auto save has expired
		CBasePlayer *pPlayer = UTIL_PlayerByIndex( 1 );

		if ( pPlayer && ( pPlayer->GetDeathTime() == 0.0f || pPlayer->GetDeathTime() > gpGlobals->curtime )
			&& !pPlayer->IsSinglePlayerGameEnding()
			)
		{
			if( pPlayer->GetHealth() >= m_fAutoSaveDangerousMinHealthToCommit )
			{
				// The player isn't dead, so make the dangerous auto save safe
				engine->ServerCommand( "autosavedangerousissafe\n" );
			}
		}

		m_fAutoSaveDangerousTime = 0.0f;
		m_fAutoSaveDangerousMinHealthToCommit = 0.0f;
	}
}

