//--------------------------------------------------------------------------------------
//
//
//
//
//--------------------------------------------------------------------------------------

#ifndef MKS_KARTMOVEMENT_H
#define MKS_KARTMOVEMENT_H



class CMKSKart;
class CMKSPlayer;

class CMKSKartMovement : public IMotionEvent
{
public:

	CMKSKartMovement();
	~CMKSKartMovement();

	//Create a new Movement class.
	static CMKSKartMovement *CreateKartMovement( CMKSKart *pKart );

	//Physics Simulation Frame
	virtual simresult_e	Simulate( IPhysicsMotionController *pController, IPhysicsObject *pObject, float deltaTime, Vector &linear, AngularImpulse &angular );

	virtual void CalculateVelocity( void );
	virtual void CalculateAngularVelocity( void );

	virtual void CalculateGravity( void );

	virtual CMKSKart *GetKart() { return m_pKart; };
	virtual void SetKart( CMKSKart *pKart ) { m_pKart = pKart; };

	virtual void DrawDebugLines();
	
	virtual CMKSPlayer *GetDriver();
public:
	bool m_bActive;
	float m_flFrametime;

	CMKSKart *m_pKart;

	Vector m_vFinalVelocity;

	IPhysicsMotionController *m_pController;
};

AngularImpulse MKSComputeRotSpeedToAlignAxes( const Vector &testAxis, const Vector &alignAxis, const AngularImpulse &currentSpeed, 
										  float damping, float scale, float maxSpeed );

#endif //MKS_KARTMOVEMENT_H