#include "cbase.h"
#include "vcollide_parse.h"
#include "bone_setup.h"

#include "mks/Kart/mks_cube.h"
#include "mks/Rules/mks_cvar_settings.h"
#include "mks/Player/mks_player.h"
#include "mks/Kart/mks_kartmovement.h"

#define MKS_KART_MODEL "models/characters/playable/kart/kart.mdl"
//#define MKS_KART_MODEL "models/props/trees/tree.mdl"

ConVar mks_cube_damping( "mks_cube_damping", "0.01", FCVAR_REPLICATED );
ConVar mks_cube_dragCo( "mks_cube_dragCo", "1.0", FCVAR_REPLICATED );
ConVar mks_cube_rotDrag( "mks_cube_rotDrag", "100.0", FCVAR_REPLICATED );
ConVar mks_cube_mass( "mks_cube_mass", "4.0", FCVAR_REPLICATED );
ConVar mks_cube_rotDamp( "mks_cube_rotDamp", "50.0", FCVAR_REPLICATED );
ConVar mks_cube_rotIner( "mks_cube_rotIner", "0.2", FCVAR_REPLICATED );
ConVar mks_cube_inertiax( "mks_cube_inertiax", "3", FCVAR_REPLICATED );
ConVar mks_cube_inertiay( "mks_cube_inertiay", "4", FCVAR_REPLICATED );
ConVar mks_cube_inertiaz( "mks_cube_inertiaz", "1", FCVAR_REPLICATED );

IMPLEMENT_SERVERCLASS_ST(CMKSCube, DT_MKSCube)
	SendPropVector( SENDINFO( m_vCubeMins ), 0, SPROP_NOSCALE ),
	SendPropVector( SENDINFO( m_vCubeMaxs ), 0, SPROP_NOSCALE ),
	SendPropVector( SENDINFO( m_vCubeScale ), 0, SPROP_NOSCALE ),

	SendPropFloat( SENDINFO( m_flDamping ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
	SendPropFloat( SENDINFO( m_flRotDamping ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
	SendPropFloat( SENDINFO( m_flDrag ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
	SendPropFloat( SENDINFO( m_flRotDrag ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
	SendPropFloat( SENDINFO( m_flMass ), 10, SPROP_NOSCALE, 0.0, 50.0f ),
	SendPropFloat( SENDINFO( m_flInertiaX ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
	SendPropFloat( SENDINFO( m_flInertiaY ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
	SendPropFloat( SENDINFO( m_flInertiaZ ), 10, SPROP_NOSCALE, -10.0f, 10.0f ),
END_SEND_TABLE()

LINK_ENTITY_TO_CLASS( mks_cube, CMKSCube );


CMKSCube::CMKSCube()
{
	m_vCubeScale = Vector( 1.0f, 1.0f, 1.0f );
	m_vCubeMins = Vector( -1.0, -1.0f, -1.0f );
	m_vCubeMaxs = Vector( 1.0f, 1.0f, 1.0f );

	m_vSpawnOrigin = Vector(0,0,0);
}


CMKSCube::~CMKSCube()
{

}


void CMKSCube::Precache( void )
{
	BaseClass::Precache();
}


void CMKSCube::Spawn( void )
{
	//Precache();

	//BaseClass::Spawn();

}


CMKSCube *CMKSCube::CreateCube( CMKSPlayer *pOwner, const char *szModel )
{
	CMKSCube *pCube = CREATE_ENTITY( CMKSCube, "mks_cube" );

	if ( !pCube )
		return NULL;

	pCube->SetOwnerEntity( pOwner );

	pCube->m_szCubeModel = (char *)szModel;
	pCube->Spawn();


#ifdef DEBUG
	Warning( "Entity Created - \"mks_cube\" \n"  );
#endif

	return pCube;
}


//Initialize Physics Cube
bool CMKSCube::InitializePhysics( void )
{	
	//Get the Min/Max Vectors of the current model.
	CreateMinMax();

	Vector origin = GetSpawnOrigin();//GetAbsOrigin();

	// create a normal physics object
	IPhysicsObject *pCube = PhysModelCreateOBB( this, m_vCubeMins, m_vCubeMaxs, origin, GetAbsAngles(), false );
	if ( !pCube )
	{
		// failed to create a physics object
		Warning(" CMKSCube::CreateCube: PhysModelCreateBox() failed for %s.\n", STRING(GetModelName()) );
		return false;
	}

	//Destroy any previous object
	VPhysicsDestroyObject();
	VPhysicsSetObject( pCube );

	SetSolid( SOLID_VPHYSICS );
	SetSolidFlags( 0 );
	SetMoveType( MOVETYPE_VPHYSICS );


	pCube->Wake();
	pCube->EnableCollisions(true);

	// We want touch calls when we hit the world
	unsigned int flags = pCube->GetCallbackFlags();
	pCube->SetCallbackFlags( flags | CALLBACK_GLOBAL_TOUCH_STATIC );

	SetCollisionGroup( COLLISION_GROUP_WEAPON );
	SetBlocksLOS( false );

	return true;
}


void CMKSCube::CreateMinMax()
{
	//Get the Min/Max of model
	const model_t *mod = GetModel();
	if ( !mod )
	{
		Warning("CMKSCube::CreateCube: GetModel failed for entity %i.\n", GetModelIndex() );
		return;
	}

	Vector mins, maxs;
	modelinfo->GetModelBounds( mod, mins, maxs );
	SetCollisionBounds( mins, maxs );
	
	m_vCubeMins = mins;
	m_vCubeMaxs = maxs;
}


void CMKSCube::SetPhysicsParams( void )
{
	IPhysicsObject *pCube = VPhysicsGetObject();
	if(!pCube)
		return;

	float damping = m_flDamping = mks_cube_damping.GetFloat();
	float rotdamping = m_flRotDamping = mks_cube_rotDamp.GetFloat();
	float drag = m_flDrag = mks_cube_dragCo.GetFloat();
	float rotdrag = m_flRotDrag = mks_cube_rotDrag.GetFloat();
	m_flMass = mks_cube_mass.GetFloat();
	m_flInertiaX = mks_cube_inertiax.GetFloat();
	m_flInertiaY = mks_cube_inertiay.GetFloat();
	m_flInertiaZ = mks_cube_inertiaz.GetFloat();

	pCube->SetDamping( &damping, &rotdamping );
	pCube->SetDragCoefficient( &drag, &rotdrag );
	pCube->SetInertia( Vector( m_flInertiaX, m_flInertiaY, m_flInertiaZ ) );
	pCube->SetMass( m_flMass );
}


CON_COMMAND( create_cube, "Create an MKS Cube" )
{
	//Get player who called command.
	CMKSPlayer *mksPlayer = ToMKSPlayer( UTIL_GetCommandClient() );
	if( !mksPlayer )
		return;

	CMKSCube::CreateCubeInFront( mksPlayer );
}

CMKSCube *CMKSCube::CreateCubeInFront( CMKSPlayer *mksPlayer )
{
	//Set to 100 units unless we specify a distance in the command.
	float fDistance = 200.0f;
	//if( args.ArgC() > 1 )
	//	fDistance = atof(args[1]); //Convert char to float

	//Get player who called command.
	//CMKSPlayer *pPlayer = ToMKSPlayer( UTIL_GetCommandClient() );

	if( !mksPlayer )
		return NULL;

	//Get position and EyeVectors (forward, right, up)
	Vector vOrigin = mksPlayer->GetAbsOrigin();
	Vector vForward, vUp, vRight;
	mksPlayer->EyeVectors( &vForward, &vRight, &vUp );

	//Create a point from origin through forward vector fDistance units.
	Vector vCubeOrigin = vOrigin + (vUp * fDistance);

	QAngle angAngle = QAngle(0,0,0);
	CMKSCube *pCube = CMKSCube::CreateCube( mksPlayer, MKS_KART_MODEL );
	if( !pCube )
		return NULL;

	pCube->SetAbsOrigin( vCubeOrigin );

	IPhysicsObject *pPhysCube = pCube->VPhysicsGetObject();
	pPhysCube->SetPosition( vCubeOrigin, angAngle, true );

	return pCube;
}